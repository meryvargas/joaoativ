﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapioca
{
    /// <summary>
    /// Lógica interna para FrmCadFunc.xaml
    /// </summary>
    public partial class FrmCadFunc : Window
    {
        string codigoEmp;
        public FrmCadFunc()
        {
            InitializeComponent();
            BtnNovo.Focus();
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            menu frm = new menu();
            frm.Show();
            this.Close();
        }        

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            BtnNovo.IsEnabled = false;
            BtnLimpar.IsEnabled = true;

            CmbEmpresa.IsEnabled = true;
            CmbEmpresa.Focus();
            
        }
        
        private void CmbEmpresa_Loaded(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();

            string selecionar = "SELECT idEmpresa, nome FROM empresa";
            MySqlCommand com = new MySqlCommand(selecionar, bd.conexao);

            MySqlDataAdapter da = new MySqlDataAdapter(com);
            DataTable dt = new DataTable();
            da.Fill(dt);
            CmbEmpresa.DisplayMemberPath = "nome";
            CmbEmpresa.ItemsSource = dt.DefaultView;
        }

        private void CmbEmpresa_KeyDown(object sender, KeyEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            MySqlCommand comm = new MySqlCommand("SELECT * FROM empresa WHERE nome = ?", bd.conexao);
            comm.Parameters.Clear();
            comm.Parameters.Add("@nome", MySqlDbType.String).Value = CmbEmpresa.Text;

            comm.CommandType = CommandType.Text;

            MySqlDataReader dr = comm.ExecuteReader();
            dr.Read();

            codigoEmp = dr.GetString(0);

            TxbNome.IsEnabled = true;
            TxbNome.Focus();
        }


        private void TxbNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbEnd.IsEnabled = true;
                TxbEnd.Focus();
            }
        }

        private void TxbEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbNumero.IsEnabled = true;
                TxbNumero.Focus();
            }
        }

        private void TxbNumero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbComplemento.IsEnabled = true;
                TxbComplemento.Focus();
            }
        }

        private void TxbComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbBairro.IsEnabled = true;
                TxbBairro.Focus();
            }
        }

        private void TxbBairro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbCid.IsEnabled = true;
                TxbCid.Focus();
            }
        }

        private void TxbCid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbUf.IsEnabled = true;
                TxbUf.Focus();
            }
        }

        private void TxbUf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbCep.IsEnabled = true;
                TxbCep.Focus();
            }
        }

        private void TxbCep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbTel1.IsEnabled = true;
                TxbTel1.Focus();
            }
        }

        private void TxbTel1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbCelular.IsEnabled = true;
                TxbCelular.Focus();
            }
        }

        private void TxbCelular_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbEmail.IsEnabled = true;
                TxbEmail.Focus();
            }
        }

        private void TxbEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbCargo.IsEnabled = true;
                TxbCargo.Focus();
            }
        }


        private void TxbCargo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbDataAdmissao.IsEnabled = true;
                TxbDataAdmissao.Focus();
            }
            
        }

        private void TxbDataAdmissao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbDataDemissao.IsEnabled = true;
                TxbDataDemissao.Focus();
            }
        }

        private void TxbDataDemissao_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbSenha.IsEnabled = true;
                TxbSenha.Focus();
            }
        }

        private void TxbSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnSalvar.IsEnabled = true;
                BtnSalvar.Focus();

                TxbDataCadFuncionario.Text = DateTime.Now.ToShortDateString();
                ChkStatusFunc.IsEnabled = true;
                MessageBox.Show("Não esqueça do status do funcionario!");
            }
        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            string status;

            DateTime DataCadastro = DateTime.Today;
            Banco bd = new Banco();
            bd.Conectar();

            if (ChkStatusFunc.IsChecked == true)
            {
                status = "ATIVO";

                string inserir = "INSERT INTO funcionario(idEmpresa,nome,endereco,numero,complemento,bairro,cidade,uf,cep,telefone,celular,email,cargo,dataAdmissao,dataDemissao,senha,statusFuncionario,dataCadFuncionario)VALUES('"
                 + codigoEmp + "','"
                 + TxbNome.Text + "','"
                + TxbEnd.Text + "','"
                + TxbNumero.Text + "','"
                + TxbComplemento.Text + "','"
                + TxbBairro.Text + "','"
                + TxbCid.Text + "','"
                + TxbUf.Text + "','"
                + TxbCep.Text + "','"
                + TxbTel1.Text + "','"
                + TxbCelular.Text + "','"
                + TxbEmail.Text + "','"
                + TxbCargo.Text + "','"
                + TxbDataAdmissao.Text + "','"
               + TxbDataDemissao.Text + "','"
               + TxbSenha.Text + "','"
                + status + "','"
                + DataCadastro.ToString("yyyy-MM--dd") + "')";

                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();
            }
            else {
                status = "INATIVO";

                string inserir = "INSERT INTO funcionario(idEmpresa,nome,endereco,numero,complemento,bairro,cidade,uf,cep,telefone,celular,email,cargo,dataAdmissao,dataDemissao,senha,statusFuncionario,dataCadFuncionario)VALUES('"
                 + codigoEmp + "','"
                 + TxbNome.Text + "','"
                + TxbEnd.Text + "','"
                + TxbNumero.Text + "','"
                + TxbComplemento.Text + "','"
                + TxbBairro.Text + "','"
                + TxbCid.Text + "','"
                + TxbUf.Text + "','"
                + TxbCep.Text + "','"
                + TxbTel1.Text + "','"
                + TxbCelular.Text + "','"
                + TxbEmail.Text + "','"
                + TxbCargo.Text + "','"
                + TxbDataAdmissao.Text + "','"
               + TxbDataDemissao.Text + "','"
               + TxbSenha.Text + "','"
                + status + "','"
                + DataCadastro.ToString("yyyy-MM--dd") + "')";

                MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
                comandos.ExecuteNonQuery();

            }
            bd.Desconectar();

            MessageBox.Show("Funcionario cadastrado com sucesso!!", "Cadastro de Funcionario");
            BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            //desbilitando os campos
            CmbEmpresa.IsEnabled = false;
            TxbNome.IsEnabled = false;
            TxbEnd.IsEnabled = false;
            TxbNumero.IsEnabled = false;
            TxbComplemento.IsEnabled = false;
            TxbBairro.IsEnabled = false;
            TxbCid.IsEnabled = false;
            TxbUf.IsEnabled = false;
            TxbCep.IsEnabled = false;
            TxbTel1.IsEnabled = false;
            TxbCelular.IsEnabled = false;
            TxbEmail.IsEnabled = false;
            TxbCargo.IsEnabled = false;
            TxbDataAdmissao.IsEnabled = false;
            TxbDataDemissao.IsEnabled = false;
            TxbSenha.IsEnabled = false;
            TxbDataCadFuncionario.IsEnabled = false;
            ChkStatusFunc.IsEnabled = false;
            //desabilitando e habilitando o necessario
            BtnSalvar.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            //retirando conteudo dos campos
            CmbEmpresa.SelectedIndex = -1;
            TxbNome.Clear();
            TxbEnd.Clear();
            TxbNumero.Clear();
            TxbComplemento.Clear();
            TxbBairro.Clear();
            TxbCid.Clear();
            TxbUf.Clear();
            TxbCep.Clear();
            TxbTel1.Clear();
            TxbCelular.Clear();
            TxbEmail.Clear();
            TxbCargo.Clear();
            TxbDataAdmissao.Clear();
            TxbDataDemissao.Clear();
            TxbSenha.Clear();
            TxbDataCadFuncionario.Clear();
            ChkStatusFunc.IsChecked = false;

        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            //desbilitando os campos
            CmbEmpresa.IsEnabled = false;
            TxbNome.IsEnabled = false;
            TxbEnd.IsEnabled = false;
            TxbNumero.IsEnabled = false;
            TxbComplemento.IsEnabled = false;
            TxbBairro.IsEnabled = false;
            TxbCid.IsEnabled = false;
            TxbUf.IsEnabled = false;
            TxbCep.IsEnabled = false;
            TxbTel1.IsEnabled = false;
            TxbCelular.IsEnabled = false;
            TxbEmail.IsEnabled = false;
            TxbCargo.IsEnabled = false;
            TxbDataAdmissao.IsEnabled = false;
            TxbDataDemissao.IsEnabled = false;
            TxbSenha.IsEnabled = false;
            TxbDataCadFuncionario.IsEnabled = false;
            ChkStatusFunc.IsEnabled = false;

            //retirando conteudo dos campos
            CmbEmpresa.SelectedIndex = -1;
            TxbNome.Clear();
            TxbEnd.Clear();
            TxbNumero.Clear();
            TxbComplemento.Clear();
            TxbBairro.Clear();
            TxbCid.Clear();
            TxbUf.Clear();
            TxbCep.Clear();
            TxbTel1.Clear();
            TxbCelular.Clear();
            TxbEmail.Clear();
            TxbCargo.Clear();
            TxbDataAdmissao.Clear();
            TxbDataDemissao.Clear();
            TxbSenha.Clear();
            TxbDataCadFuncionario.Clear();
            ChkStatusFunc.IsChecked = false;

            BtnSalvar.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
           
            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
            
        }

    }
}
