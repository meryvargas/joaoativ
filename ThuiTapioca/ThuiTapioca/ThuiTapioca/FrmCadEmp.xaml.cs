﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThuiTapioca
{
    /// <summary>
    /// Lógica interna para FrmCadEmp.xaml
    /// </summary>
    public partial class FrmCadEmp : Window
    {
        public FrmCadEmp()
        {
            InitializeComponent();
            BtnNovo.Focus();
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            menu frm = new menu();
            frm.Show();
            this.Close();
        }

        private void BtnNovo_Click(object sender, RoutedEventArgs e)
        {
            BtnNovo.IsEnabled = false;
            BtnLimpar.IsEnabled=true;
            TxbNome.IsEnabled = true;
            TxbNome.Focus();
        }

        private void TxbNome_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key==Key.Enter)
            {
                TxbEnd.IsEnabled = true;
                TxbEnd.Focus();
            }
        }

        private void TxbEnd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbNumero.IsEnabled = true;
                TxbNumero.Focus();
            }
        }

        private void TxbNumero_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbComplemento.IsEnabled = true;
                TxbComplemento.Focus();
            }
        }
        
        private void TxbComplemento_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbBairro.IsEnabled = true;
                TxbBairro.Focus();
            }
        }

        private void TxbBairro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbCid.IsEnabled = true;
                TxbCid.Focus();
            }
        }

        private void TxbCid_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbUf.IsEnabled = true;
                TxbUf.Focus();
            }
        }

        private void TxbUf_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbCep.IsEnabled = true;
                TxbCep.Focus();
            }
        }

        private void TxbCep_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbTel1.IsEnabled = true;
                TxbTel1.Focus();
            }
        }

        private void TxbTel1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbTel2.IsEnabled = true;
                TxbTel2.Focus();
            }
        }

        private void TxbTel2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbEmail.IsEnabled = true;
                TxbEmail.Focus();
            }
        }

        private void TxbEmail_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbDataAbertura.IsEnabled = true;
                TxbDataAbertura.Focus();
            }
        }

        private void TxbDataAbertura_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                TxbDataFim.IsEnabled = true;
                TxbDataFim.Focus();
            }
        }

        private void TxbDataFim_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                BtnSalvar.IsEnabled = true;
                BtnSalvar.Focus();

                TxbDataCadEmpresa.Text = DateTime.Now.ToShortDateString();

            }
        }

        private void BtnSalvar_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            DateTime DataCadastro = DateTime.Today;

            string inserir = "INSERT INTO empresa(nome,endereco,numero,complemento,bairro,cidade,uf,cep,telefone1,telefone2,email,dataAbertura,dataFim,dataCadEmpresa)VALUES('"
                + TxbNome.Text + "','"
                + TxbEnd.Text + "','"
                + TxbNumero.Text + "','"
                + TxbComplemento.Text + "','"
                + TxbBairro.Text + "','"
                + TxbCid.Text + "','"
                + TxbUf.Text + "','"
                + TxbCep.Text + "','"
                + TxbTel1.Text + "','"
                + TxbTel2.Text + "','"
                + TxbEmail.Text + "','"
                + TxbDataAbertura.Text + "','"
               + TxbDataFim.Text + "','"
                + DataCadastro.ToString("yyyy-MM--dd") + "')";

            MySqlCommand comandos = new MySqlCommand(inserir, bd.conexao);
            comandos.ExecuteNonQuery();

            bd.Desconectar();

            MessageBox.Show("Empresa cadastrada com sucesso!!", "Cadastro de Empresa");
            BtnLimpar.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
            //desbilitando os campos
            TxbNome.IsEnabled = false;
            TxbEnd.IsEnabled = false;
            TxbNumero.IsEnabled = false;
            TxbComplemento.IsEnabled = false;
            TxbBairro.IsEnabled = false;
            TxbCid.IsEnabled = false;
            TxbUf.IsEnabled = false;
            TxbCep.IsEnabled = false;
            TxbTel1.IsEnabled = false;
            TxbTel2.IsEnabled = false;
            TxbEmail.IsEnabled = false;
            TxbDataAbertura.IsEnabled = false;
            TxbDataFim.IsEnabled = false;
            TxbDataCadEmpresa.IsEnabled = false;
            //desabilitando e habilitando o necessario
            BtnSalvar.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            //retirando conteudo dos campos
            TxbNome.Clear();
            TxbEnd.Clear();
            TxbNumero.Clear();
            TxbComplemento.Clear();
            TxbBairro.Clear();
            TxbCid.Clear();
            TxbUf.Clear();
            TxbCep.Clear();
            TxbTel1.Clear();
            TxbTel2.Clear();
            TxbEmail.Clear();
            TxbDataAbertura.Clear();
            TxbDataFim.Clear();
            TxbDataCadEmpresa.Clear();
        }

        private void BtnLimpar_Click(object sender, RoutedEventArgs e)
        {
            TxbNome.IsEnabled = false;
            TxbEnd.IsEnabled = false;
            TxbNumero.IsEnabled = false;
            TxbComplemento.IsEnabled = false;
            TxbBairro.IsEnabled = false;
            TxbCid.IsEnabled = false;
            TxbUf.IsEnabled = false;
            TxbCep.IsEnabled = false;
            TxbTel1.IsEnabled = false;
            TxbTel2.IsEnabled = false;
            TxbEmail.IsEnabled = false;
            TxbDataAbertura.IsEnabled = false;
            TxbDataFim.IsEnabled = false;
            TxbDataCadEmpresa.IsEnabled = false;
            //desabilitando e habilitando o necessario
            BtnSalvar.IsEnabled = false;
            BtnLimpar.IsEnabled = false;
            BtnNovo.IsEnabled = true;
            //retirando conteudo dos campos
            TxbNome.Clear();
            TxbEnd.Clear();
            TxbNumero.Clear();
            TxbComplemento.Clear();
            TxbBairro.Clear();
            TxbCid.Clear();
            TxbUf.Clear();
            TxbCep.Clear();
            TxbTel1.Clear();
            TxbTel2.Clear();
            TxbEmail.Clear();
            TxbDataAbertura.Clear();
            TxbDataFim.Clear();
            TxbDataCadEmpresa.Clear();

            BtnNovo.IsEnabled = true;
            BtnNovo.Focus();
            BtnLimpar.IsEnabled = false;
        }
        
    }
}
