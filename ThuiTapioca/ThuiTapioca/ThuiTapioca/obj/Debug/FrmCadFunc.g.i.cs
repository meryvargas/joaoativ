﻿#pragma checksum "..\..\FrmCadFunc.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "1B745BE07E7B94ED9B1926C9AB3848B0"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;
using ThuiTapioca;


namespace ThuiTapioca {
    
    
    /// <summary>
    /// FrmCadFunc
    /// </summary>
    public partial class FrmCadFunc : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 17 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbNome;
        
        #line default
        #line hidden
        
        
        #line 28 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbEnd;
        
        #line default
        #line hidden
        
        
        #line 38 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbNumero;
        
        #line default
        #line hidden
        
        
        #line 48 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbComplemento;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbBairro;
        
        #line default
        #line hidden
        
        
        #line 68 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbCid;
        
        #line default
        #line hidden
        
        
        #line 78 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbUf;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbDataCadFuncionario;
        
        #line default
        #line hidden
        
        
        #line 98 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbTel1;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbEmail;
        
        #line default
        #line hidden
        
        
        #line 118 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbDataDemissao;
        
        #line default
        #line hidden
        
        
        #line 128 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbCep;
        
        #line default
        #line hidden
        
        
        #line 138 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbCelular;
        
        #line default
        #line hidden
        
        
        #line 148 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbDataAdmissao;
        
        #line default
        #line hidden
        
        
        #line 158 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSalvar;
        
        #line default
        #line hidden
        
        
        #line 159 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnLimpar;
        
        #line default
        #line hidden
        
        
        #line 160 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnNovo;
        
        #line default
        #line hidden
        
        
        #line 161 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button BtnSair;
        
        #line default
        #line hidden
        
        
        #line 162 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ComboBox CmbEmpresa;
        
        #line default
        #line hidden
        
        
        #line 164 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbCargo;
        
        #line default
        #line hidden
        
        
        #line 174 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TxbSenha;
        
        #line default
        #line hidden
        
        
        #line 184 "..\..\FrmCadFunc.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox ChkStatusFunc;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/ThuiTapioca;component/frmcadfunc.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\FrmCadFunc.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TxbNome = ((System.Windows.Controls.TextBox)(target));
            
            #line 17 "..\..\FrmCadFunc.xaml"
            this.TxbNome.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbNome_KeyDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.TxbEnd = ((System.Windows.Controls.TextBox)(target));
            
            #line 28 "..\..\FrmCadFunc.xaml"
            this.TxbEnd.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbEnd_KeyDown);
            
            #line default
            #line hidden
            return;
            case 3:
            this.TxbNumero = ((System.Windows.Controls.TextBox)(target));
            
            #line 38 "..\..\FrmCadFunc.xaml"
            this.TxbNumero.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbNumero_KeyDown);
            
            #line default
            #line hidden
            return;
            case 4:
            this.TxbComplemento = ((System.Windows.Controls.TextBox)(target));
            
            #line 48 "..\..\FrmCadFunc.xaml"
            this.TxbComplemento.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbComplemento_KeyDown);
            
            #line default
            #line hidden
            return;
            case 5:
            this.TxbBairro = ((System.Windows.Controls.TextBox)(target));
            
            #line 58 "..\..\FrmCadFunc.xaml"
            this.TxbBairro.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbBairro_KeyDown);
            
            #line default
            #line hidden
            return;
            case 6:
            this.TxbCid = ((System.Windows.Controls.TextBox)(target));
            
            #line 68 "..\..\FrmCadFunc.xaml"
            this.TxbCid.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbCid_KeyDown);
            
            #line default
            #line hidden
            return;
            case 7:
            this.TxbUf = ((System.Windows.Controls.TextBox)(target));
            
            #line 78 "..\..\FrmCadFunc.xaml"
            this.TxbUf.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbUf_KeyDown);
            
            #line default
            #line hidden
            return;
            case 8:
            this.TxbDataCadFuncionario = ((System.Windows.Controls.TextBox)(target));
            return;
            case 9:
            this.TxbTel1 = ((System.Windows.Controls.TextBox)(target));
            
            #line 98 "..\..\FrmCadFunc.xaml"
            this.TxbTel1.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbTel1_KeyDown);
            
            #line default
            #line hidden
            return;
            case 10:
            this.TxbEmail = ((System.Windows.Controls.TextBox)(target));
            
            #line 108 "..\..\FrmCadFunc.xaml"
            this.TxbEmail.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbEmail_KeyDown);
            
            #line default
            #line hidden
            return;
            case 11:
            this.TxbDataDemissao = ((System.Windows.Controls.TextBox)(target));
            
            #line 118 "..\..\FrmCadFunc.xaml"
            this.TxbDataDemissao.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbDataDemissao_KeyDown);
            
            #line default
            #line hidden
            return;
            case 12:
            this.TxbCep = ((System.Windows.Controls.TextBox)(target));
            
            #line 128 "..\..\FrmCadFunc.xaml"
            this.TxbCep.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbCep_KeyDown);
            
            #line default
            #line hidden
            return;
            case 13:
            this.TxbCelular = ((System.Windows.Controls.TextBox)(target));
            
            #line 138 "..\..\FrmCadFunc.xaml"
            this.TxbCelular.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbCelular_KeyDown);
            
            #line default
            #line hidden
            return;
            case 14:
            this.TxbDataAdmissao = ((System.Windows.Controls.TextBox)(target));
            
            #line 148 "..\..\FrmCadFunc.xaml"
            this.TxbDataAdmissao.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbDataAdmissao_KeyDown);
            
            #line default
            #line hidden
            return;
            case 15:
            this.BtnSalvar = ((System.Windows.Controls.Button)(target));
            
            #line 158 "..\..\FrmCadFunc.xaml"
            this.BtnSalvar.Click += new System.Windows.RoutedEventHandler(this.BtnSalvar_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.BtnLimpar = ((System.Windows.Controls.Button)(target));
            
            #line 159 "..\..\FrmCadFunc.xaml"
            this.BtnLimpar.Click += new System.Windows.RoutedEventHandler(this.BtnLimpar_Click);
            
            #line default
            #line hidden
            return;
            case 17:
            this.BtnNovo = ((System.Windows.Controls.Button)(target));
            
            #line 160 "..\..\FrmCadFunc.xaml"
            this.BtnNovo.Click += new System.Windows.RoutedEventHandler(this.BtnNovo_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.BtnSair = ((System.Windows.Controls.Button)(target));
            
            #line 161 "..\..\FrmCadFunc.xaml"
            this.BtnSair.Click += new System.Windows.RoutedEventHandler(this.BtnSair_Click);
            
            #line default
            #line hidden
            return;
            case 19:
            this.CmbEmpresa = ((System.Windows.Controls.ComboBox)(target));
            
            #line 162 "..\..\FrmCadFunc.xaml"
            this.CmbEmpresa.KeyDown += new System.Windows.Input.KeyEventHandler(this.CmbEmpresa_KeyDown);
            
            #line default
            #line hidden
            
            #line 162 "..\..\FrmCadFunc.xaml"
            this.CmbEmpresa.Loaded += new System.Windows.RoutedEventHandler(this.CmbEmpresa_Loaded);
            
            #line default
            #line hidden
            return;
            case 20:
            this.TxbCargo = ((System.Windows.Controls.TextBox)(target));
            
            #line 164 "..\..\FrmCadFunc.xaml"
            this.TxbCargo.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbCargo_KeyDown);
            
            #line default
            #line hidden
            return;
            case 21:
            this.TxbSenha = ((System.Windows.Controls.TextBox)(target));
            
            #line 174 "..\..\FrmCadFunc.xaml"
            this.TxbSenha.KeyDown += new System.Windows.Input.KeyEventHandler(this.TxbSenha_KeyDown);
            
            #line default
            #line hidden
            return;
            case 22:
            this.ChkStatusFunc = ((System.Windows.Controls.CheckBox)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

