﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace ThuiTapioca
{
    /// <summary>
    /// Interação lógica para MainWindow.xam
    /// </summary>
    public partial class MainWindow : Window
    {
        string user, senha;
        int i = 0;
        public MainWindow()
        {
            InitializeComponent();
            TxbLogin.Focus();
            
        }

        private void BtnSair_Click(object sender, RoutedEventArgs e)
        {
            var sair = MessageBox.Show("Tem certeza que deseja encerrar a aplicação?", "Encerrar aplicação", MessageBoxButton.YesNo, MessageBoxImage.Exclamation);
            if (sair != MessageBoxResult.Yes)
            {
                return;
            }
            else
            {
                Application.Current.Shutdown();
            }
        }

        private void TxbLogin_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key==Key.Enter)
            {
                user = TxbLogin.Text;
                TxbSenha.IsEnabled = true;
                TxbSenha.Focus();
            }
        }

        private void TxbSenha_KeyDown(object sender, KeyEventArgs e)
        {
            if(e.Key==Key.Enter)
            {
                senha = TxbSenha.Password;

                BtnEntrar.IsEnabled = true;
                BtnEntrar.Focus();
            }
        }

        private void BtnEntrar_Click(object sender, RoutedEventArgs e)
        {
            Banco bd = new Banco();
            bd.Conectar();
            MySqlCommand comm = new MySqlCommand("SELECT * FROM funcionario WHERE email = @usuario AND senha =@senha AND statusFuncionario='ATIVO'", bd.conexao);
            comm.Parameters.Clear();
            comm.Parameters.Add("@usuario", MySqlDbType.String).Value = TxbLogin.Text;
            comm.Parameters.Add("@senha", MySqlDbType.String).Value = TxbSenha.Password;

            try
            {

                comm.CommandType = CommandType.Text;
                MySqlDataReader dr = comm.ExecuteReader();
                dr.Read();
                //NAO ESQUECER, ele sempre pega um campo acima da ordem do banco pois começa a partir do 0
                Acesso.nivelAcesso = dr.GetString(13);
                Acesso.empresaAcesso = dr.GetString(2);
                //acesso.empresaAcesso = dr.GetString(17);

                BtnEntrar.IsEnabled = false;
                Thread.Sleep(10);
                i = 0;
                Task.Run(() =>
                {
                    while (i < 100)
                    {
                        i++;
                        Thread.Sleep(50);
                        this.Dispatcher.Invoke(() => //usar para att imediata obrigatoria

                        {
                            //PgbLogin.Value = i;<-animacaoAPLICARNAOESQUECER

                            while (i == 100)
                            {
                                user = Acesso.empresaAcesso;

                                if (Acesso.nivelAcesso == "ADM")
                                {
                                    Hide();
                                    menu frm = new menu();
                                    frm.Show();
                                    frm.LblLogado.Content = user;
                                    break;
                                }
                                else if (Acesso.nivelAcesso == "FUNCIONARIO")
                                {
                                    Hide();
                                    menu frm = new menu();
                                    frm.Show();
                                    frm.LblLogado.Content = user;
                                    break;

                                }

                                else if (Acesso.nivelAcesso == "GERENTE")
                                {
                                    Hide();
                                    menu frm = new menu();
                                    frm.Show();
                                    frm.LblLogado.Content = user;
                                    break;
                                }
                            }
                        });
                    }
                });
            }
            catch
            {
                MessageBox.Show("Favor preencher o login ou a senha corretamente!");
                TxbLogin.Clear();
                TxbSenha.Clear();
                TxbLogin.Focus();
                TxbSenha.IsEnabled = false;
                BtnEntrar.IsEnabled = false;
            }
        }
    }
}
